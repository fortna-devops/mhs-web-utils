import pytest


@pytest.fixture(scope='function')
def jwks_path(monkeypatch):
    monkeypatch.setenv('jwks_path', 'tests/test_jwks.json')


@pytest.fixture(scope='function')
def signature_secret(monkeypatch):
    monkeypatch.setenv('secret', 'testing')
