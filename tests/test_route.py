from mhs_web_utils.response import Response
from mhs_web_utils.route import Route
from mhs_web_utils.handler import Handler


class HandlerTest(Handler):
    def handle(self, request):
        return Response()


def check_test(x):
    return True


def test_route():
    handler = HandlerTest()
    route = Route(check_test, handler)
    assert route.handler == handler
    assert route.check == check_test
