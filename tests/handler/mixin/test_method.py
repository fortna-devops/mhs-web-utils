import pytest

from mhs_web_utils.handler.mixins import MethodMixin
from mhs_web_utils.response import Response
from mhs_web_utils.request import Request


class _Handler(MethodMixin):
    allowed_methods = ('GET',)

    def get_handler(self, request):
        return Response('success')


@pytest.mark.parametrize('method, expected_code, expected_body', (
    ('GET', 200, 'success'),
    ('POST', 405, 'Method not allowed'),
))
def test_handle(method, expected_code, expected_body):
    request = Request({
        'httpMethod': method,
        'path': '/'
    })
    response = _Handler().handle(request)
    assert response._code == expected_code
    assert response._body == expected_body
