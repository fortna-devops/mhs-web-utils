from mhs_web_utils.handler.mixins import DBMixin


class _Connector:
    pass


def test_connector():
    connector = _Connector()
    handler = DBMixin(connector)
    assert handler.connector == connector
