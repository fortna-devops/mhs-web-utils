import pytest


from mhs_web_utils.authorizer import SignatureAuthorizer, AuthorizationError
from mhs_web_utils.request import Request


@pytest.mark.parametrize('data, expected_signature', (
    ({'a': 'some', 'b': 'data', 'c': 'to', 'd': 'sign'}, '9d6d9dbb9697b3a95e0144238d1fe7bec489803c96997284e91b32e8b3e107b7'),
    ({'b': 'data', 'a': 'some', 'd': 'sign', 'c': 'to'}, '9d6d9dbb9697b3a95e0144238d1fe7bec489803c96997284e91b32e8b3e107b7'),
    pytest.param({'a': 'some', 'b': 'data', 'c': 'with', 'd': None},  None, marks=pytest.mark.xfail(raises=TypeError)),
))
def test_sign(signature_secret, data, expected_signature):
    assert SignatureAuthorizer.sign(data) == expected_signature


@pytest.mark.parametrize('request_query_data', (
    {'a': 'some', 'b': 'data', 'c': 'to', 'd': 'sign', 'signature': '9d6d9dbb9697b3a95e0144238d1fe7bec489803c96997284e91b32e8b3e107b7'},
    {'b': 'data', 'a': 'some', 'd': 'sign', 'c': 'to', 'signature': '9d6d9dbb9697b3a95e0144238d1fe7bec489803c96997284e91b32e8b3e107b7'},
    pytest.param({'some_other_param': 'some_unused_value', 'a': 'some', 'b': 'data', 'c': 'to', 'd': 'sign', 'signature': '9d6d9dbb9697b3a95e0144238d1fe7bec489803c96997284e91b32e8b3e107b7'}, marks=pytest.mark.xfail(raises=AuthorizationError, reason='additional parameter')),
    pytest.param({'a': 'some', 'b': 'data', 'c': 'to', 'd': 'sign'}, marks=pytest.mark.xfail(raises=AuthorizationError, reason='no signature')),
    pytest.param({'a': 'some', 'b': 'data', 'c': 'to', 'd': 'sign', 'signature': 'invalid'}, marks=pytest.mark.xfail(raises=AuthorizationError, reason='invalid signature')),
))
def test_authorize(signature_secret, request_query_data):
    authorizer = SignatureAuthorizer()
    request = Request({
        'httpMethod': 'GET',
        'path': 'path',
        'queryStringParameters': request_query_data
    })
    authorizer.authorize(request)
