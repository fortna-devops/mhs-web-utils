import pytest

from mhs_web_utils.authorizer import CognitoSiteAuthorizer, AuthorizationError
from mhs_web_utils.validation import ValidationError
from mhs_web_utils.request import Request

from tests.authorizer.cognito.shared_data import USER_AUTH_TOKEN


@pytest.mark.parametrize('request_headers, request_query_data', (
    pytest.param({'authorization': USER_AUTH_TOKEN}, {}, marks=pytest.mark.xfail(raises=ValidationError, reason='no site field')),
    pytest.param({'authorization': USER_AUTH_TOKEN}, {'site': 'invalidcustomer-site'}, marks=pytest.mark.xfail(raises=AuthorizationError, reason='invalid customer')),
    pytest.param({'authorization': USER_AUTH_TOKEN}, {'site': 'testing-invalidsite'}, marks=pytest.mark.xfail(raises=AuthorizationError, reason='invalid site')),
    ({'authorization': USER_AUTH_TOKEN}, {'site': 'testing-site'})
))
def test_authorize(jwks_path, request_headers, request_query_data):
    authorizer = CognitoSiteAuthorizer()
    request = Request({
        'httpMethod': 'GET',
        'path': 'path',
        'headers': request_headers,
        'queryStringParameters': request_query_data,
    })
    authorizer.authorize(request)
