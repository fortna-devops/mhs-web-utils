import pytest

from mhs_web_utils.authorizer import CognitoAdminAuthorizer, AuthorizationError
from mhs_web_utils.request import Request

from tests.authorizer.cognito.shared_data import ADMIN_AUTH_TOKEN, USER_AUTH_TOKEN


@pytest.mark.parametrize('request_headers', (
    pytest.param({'authorization': USER_AUTH_TOKEN}, marks=pytest.mark.xfail(raises=AuthorizationError, reason='no admin permissions')),
    {'authorization': ADMIN_AUTH_TOKEN},
))
def test_authorize(jwks_path, request_headers):
    authorizer = CognitoAdminAuthorizer()
    request = Request({
        'httpMethod': 'GET',
        'path': 'path',
        'headers': request_headers
    })
    authorizer.authorize(request)
