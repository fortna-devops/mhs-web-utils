import pytest

from mhs_web_utils.authorizer import CognitoAuthorizer, AuthorizationError
from mhs_web_utils.request import Request

from tests.authorizer.cognito.shared_data import TOKEN_PREFIX, NO_PUB_KEY_AUTH_TOKEN, NO_KID_TOKEN,\
    VERIFICATION_FAIL_AUTH_TOKEN, VALID_AUTH_TOKEN


@pytest.mark.parametrize('request_headers', (
    pytest.param({}, marks=pytest.mark.xfail(raises=AuthorizationError, reason='no header')),
    pytest.param({'authorization': ''}, marks=pytest.mark.xfail(raises=AuthorizationError, reason='empty header')),
    pytest.param({'authorization': TOKEN_PREFIX}, marks=pytest.mark.xfail(raises=AuthorizationError, reason='no token in header')),
    pytest.param({'authorization': f'{TOKEN_PREFIX} token extra'}, marks=pytest.mark.xfail(raises=AuthorizationError, reason='extra info in header')),
    pytest.param({'authorization': f'{TOKEN_PREFIX} token'}, marks=pytest.mark.xfail(raises=AuthorizationError, reason='no "." in token')),
    pytest.param({'authorization': f'{TOKEN_PREFIX} token.tok'}, marks=pytest.mark.xfail(raises=AuthorizationError, reason='error decoding token headers')),
    pytest.param({'authorization': NO_KID_TOKEN}, marks=pytest.mark.xfail(raises=AuthorizationError, reason='key id not found')),
    pytest.param({'authorization': NO_PUB_KEY_AUTH_TOKEN}, marks=pytest.mark.xfail(raises=AuthorizationError, reason='pub key not found')),
    pytest.param({'authorization': VERIFICATION_FAIL_AUTH_TOKEN}, marks=pytest.mark.xfail(raises=AuthorizationError, reason='pub key verification failed')),
    {'authorization': VALID_AUTH_TOKEN},
))
def test_authorize(jwks_path, request_headers):
    authorizer = CognitoAuthorizer()
    request = Request({
        'httpMethod': 'GET',
        'path': 'path',
        'headers': request_headers
    })
    authorizer.authorize(request)


def test_token_no_auth():
    authorizer = CognitoAuthorizer()
    with pytest.raises(AuthorizationError) as e:
        _ = authorizer.token
    assert str(e.value) == 'You need to call "authorize" method at first'


def test_token(jwks_path):
    authorizer = CognitoAuthorizer()
    request = Request({
        'httpMethod': 'GET',
        'path': 'path',
        'headers': {'authorization': VALID_AUTH_TOKEN}
    })
    authorizer.authorize(request)
    assert authorizer.token is not None
