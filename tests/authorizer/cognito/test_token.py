import pytest

from mhs_web_utils.authorizer.cognito.token import Token


def test_data():
    data = {'a': 'b'}
    token = Token(data)
    assert token.data == data


@pytest.mark.parametrize('data, expected_groups', (
    ({}, []),
    ({'cognito:groups': []}, []),
    ({'cognito:groups': ['test']}, ['test']),
    ({'cognito:groups': ['Test']}, ['test']),
    ({'cognito:groups': ['test', 'test2']}, ['test', 'test2']),
    ({'cognito:groups': ['dhl']}, ['dhl-miami']),
))
def test_groups(data, expected_groups):
    token = Token(data)
    assert sorted(token.groups) == sorted(expected_groups)


@pytest.mark.parametrize('data, expected_customers', (
    ({'cognito:groups': ['test']}, []),
    ({'cognito:groups': ['test-site']}, ['test']),
    ({'cognito:groups': ['test-site', 'test-site2']}, ['test']),
    ({'cognito:groups': ['test-site', 'test2-site']}, ['test', 'test2']),
))
def test_customers(data, expected_customers):
    token = Token(data)
    assert sorted(token.customers) == sorted(expected_customers)


def test_has_admin_permissions():
    data = {'cognito:groups': ['admin']}
    token = Token(data)
    assert token.has_admin_permissions


def test_has_no_admin_permissions():
    data = {'cognito:groups': ['not_admin']}
    token = Token(data)
    assert not token.has_admin_permissions


def test_username():
    username = 'test'
    data = {'cognito:username': username}
    token = Token(data)
    assert token.user_name == username


@pytest.mark.parametrize('data, expected_groups', (
    ({'cognito:groups': ['testcustomer-testsite']}, ['testcustomer-testsite']),
    ({'cognito:groups': ['testcustomer-testsite', 'testcustomer2-testsite']}, ['testcustomer-testsite']),
))
def test_get_customer_groups(data, expected_groups):
    token = Token(data)
    assert sorted(token.get_customer_groups('testcustomer')) == sorted(expected_groups)
