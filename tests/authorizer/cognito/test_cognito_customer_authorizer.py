import pytest

from mhs_web_utils.authorizer import CognitoCustomerAuthorizer, AuthorizationError
from mhs_web_utils.validation import ValidationError
from mhs_web_utils.request import Request

from tests.authorizer.cognito.shared_data import USER_AUTH_TOKEN


@pytest.mark.parametrize('request_headers, request_query_data', (
    pytest.param({'authorization': USER_AUTH_TOKEN}, {}, marks=pytest.mark.xfail(raises=ValidationError, reason='no customer field')),
    pytest.param({'authorization': USER_AUTH_TOKEN}, {'customer': 'invalidcustomer'}, marks=pytest.mark.xfail(raises=AuthorizationError, reason='invalid customer')),
    ({'authorization': USER_AUTH_TOKEN}, {'customer': 'testing'})
))
def test_authorize(jwks_path, request_headers, request_query_data):
    authorizer = CognitoCustomerAuthorizer()
    request = Request({
        'httpMethod': 'GET',
        'path': 'path',
        'headers': request_headers,
        'queryStringParameters': request_query_data,
    })
    authorizer.authorize(request)
