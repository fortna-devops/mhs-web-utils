import pytest

from mhs_web_utils.response.base import Response


@pytest.mark.parametrize('body, status_code, headers, expected_result', (
    ('some_body', 200, {'Test-Header': 'value'}, {'statusCode': '200', 'body': 'some_body', 'headers': {'Access-Control-Allow-Origin': '*', 'Content-Type': 'text/plain', 'Test-Header': 'value'}}),
    ('some_body', 200, {}, {'statusCode': '200', 'body': 'some_body', 'headers': {'Access-Control-Allow-Origin': '*', 'Content-Type': 'text/plain'}}),
    ('some_body', 200, None, {'statusCode': '200', 'body': 'some_body', 'headers': {'Access-Control-Allow-Origin': '*', 'Content-Type': 'text/plain'}}),
))
def test_to_dict(body, status_code, headers, expected_result):
    assert Response(body=body, code=status_code, headers=headers).to_dict() == expected_result
