import enum
from decimal import Decimal
from datetime import datetime, date, timedelta

import pytest


from mhs_web_utils.response.json import Encoder, JSONResponse


class EnumTest(enum.Enum):
    some_attr = 'some_value'


@pytest.mark.parametrize('input_value, expected_value', (
    (Decimal('0.1'), 0.1),
    (EnumTest.some_attr, 'some_attr'),
    (timedelta(days=1), 86400),
    (datetime(year=2020, month=10, day=6, hour=13, minute=47, second=7, microsecond=123), '2020-10-06 13:47:07.000'),
    (date(year=2020, month=10, day=6), '2020-10-06'),
    pytest.param(object, None, marks=pytest.mark.xfail(raises=TypeError))
))
def test_encoder(input_value, expected_value):
    assert Encoder().default(input_value) == expected_value


@pytest.mark.parametrize('input_value, expected_value', (
    ([1, 2, 3], '[1, 2, 3]'),
    ({'a': [1, 2, 3]}, '{"a": [1, 2, 3]}'),
))
def test_json_response(input_value, expected_value):
    response = JSONResponse(input_value)
    assert response._body == expected_value
    assert response._headers['Content-Type'] == 'application/json'
