import pytest

from mhs_web_utils.response.generic import BadRequestResponse, UnauthorizedResponse, ForbiddenResponse, \
    NotFoundResponse, MethodNotAllowedResponse


@pytest.mark.parametrize('response_cls, status_code, body', (
    (BadRequestResponse, 400, 'BadRequest'),
    (UnauthorizedResponse, 401, 'Unauthorized'),
    (ForbiddenResponse, 403, 'Forbidden'),
    (NotFoundResponse, 404, 'Not Found'),
    (MethodNotAllowedResponse, 405, 'Method not allowed'),
))
def test_generic_response(response_cls, status_code, body):
    response = response_cls()
    assert response._code == status_code
    assert response._body == body
