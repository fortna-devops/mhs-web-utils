import pytest

from mhs_web_utils.request import Request


@pytest.mark.parametrize('event, method, path, query, headers, body', (
    ({'httpMethod': 'GET', 'path': '/', 'queryStringParameters': {'a': 'b'}, 'headers': {'Test-Header': 'value'}, 'body': 'body_value'}, 'GET', '/', {'a': 'b'}, {'test-header': 'value'}, 'body_value'),
    ({'httpMethod': 'GET', 'path': '/', 'queryStringParameters': {'a': 'b'}, 'body': 'body_value'}, 'GET', '/', {'a': 'b'}, {}, 'body_value'),
    ({'httpMethod': 'GET', 'path': '/', 'headers': {'Test-Header': 'value'}, 'body': 'body_value'}, 'GET', '/', {}, {'test-header': 'value'}, 'body_value'),
    ({'httpMethod': 'GET', 'path': '/', 'queryStringParameters': {'a': 'b'}, 'headers': {'Test-Header': 'value'}}, 'GET', '/', {'a': 'b'}, {'test-header': 'value'}, ''),
))
def test_request(event, method, path, query, headers, body):
    request = Request(event)

    assert request.event == event
    assert request.method == method
    assert request.path == path
    assert request.query == query
    assert request.headers == headers
    assert request.body == body
