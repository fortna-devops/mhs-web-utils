import pytest

from mhs_web_utils.route import Route
from mhs_web_utils.resolver import Resolver
from mhs_web_utils.response import Response
from mhs_web_utils.handler import Handler
from mhs_web_utils.authorizer import Authorizer, AuthorizationError
from mhs_web_utils.validation import ValidationError


class AuthorizedDummyAuthorizer(Authorizer):
    def authorize(self, request):
        pass


class UnauthorizedDummyAuthorizer(Authorizer):
    def authorize(self, request):
        raise AuthorizationError()


class OkHandler(Handler):
    authorizer_cls = AuthorizedDummyAuthorizer

    def handle(self, request):
        return Response()


class ExceptionHandler(OkHandler):
    def handle(self, request):
        _ = 1/0
        return Response()


class InvalidDataHandler(OkHandler):
    def handle(self, request):
        raise ValidationError({'field': 'error'})


class UnauthorizedHandler(OkHandler):
    authorizer_cls = UnauthorizedDummyAuthorizer


@pytest.mark.parametrize('route, status_code, debug', (
    (Route(lambda path: True, OkHandler()), 200, True),
    (Route(lambda path: False, OkHandler()), 404, True),
    (Route(lambda path: True, UnauthorizedHandler()), 403, True),
    (Route(lambda path: True, ExceptionHandler()), 502, True),
    pytest.param(Route(lambda path: True, ExceptionHandler()), 0, False, marks=pytest.mark.xfail(raises=ZeroDivisionError)),
    (Route(lambda path: True, InvalidDataHandler()), 400, True),
))
def test_resolver(route, status_code, debug):
    response = Resolver(route, debug=debug).process({'httpMethod': 'GET', 'path': '/'}, {})
    assert response['statusCode'] == str(status_code)
