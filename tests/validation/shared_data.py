import base64
from collections import namedtuple
from datetime import datetime, timedelta
from pathlib import PurePath


from mhs_web_utils.validation import AnyField, StrField, IntField, FloatField, BoolField, PathField, SiteField, \
    DateTimeField, TimedeltaField, AttrField, ListField, TupleField, Base64EncodedField, NestedDataField, Validator


ParametrizedTestCase = namedtuple('TestCaseData', [
    'field_cls',
    'field_kwargs',
    'valid_input_value',
    'expected_value',
    'invalid_input_value',
    'expected_error',
    'default_value',
])


class AttrTest:
    some_attr = 'some_value'


class NestedValidator(Validator):
    nested_field = StrField()


any_field_test_case = ParametrizedTestCase(
    AnyField, {}, 'test_value', 'test_value', 'invalid_value', None, 'default_value'
)
str_field_test_case = ParametrizedTestCase(
    StrField, {}, 'test_value', 'test_value', 'invalid_value', None, 'default_value'
)
int_field_test_case = ParametrizedTestCase(
    IntField, {}, '1', 1, 'invalid_value', 'is not int', 2
)
float_field_test_case = ParametrizedTestCase(
    FloatField, {}, '1.1', 1.1, 'invalid_value', 'is not float', 2.1
)
bool_field_test_case = ParametrizedTestCase(
    BoolField, {}, 'true', True, 'invalid_value', 'is not bool', False
)
path_field_test_case = ParametrizedTestCase(
    PathField, {}, 'some/path', PurePath('some/path'), 'invalid_value', None, PurePath('default/path')
)
site_field_test_case = ParametrizedTestCase(
    SiteField, {}, 'some-site', 'some_site', 'invalid/site', 'Must contain "-" or "_"', 'default_site'
)
site_underscore_field_test_case = ParametrizedTestCase(
    SiteField, {}, 'some_site', 'some_site', 'invalid/site', 'Must contain "-" or "_"', 'default_site'
)
datetime_field_test_case = ParametrizedTestCase(
    DateTimeField, {}, '2020-04-01T11:33:52.139Z', datetime(2020, 4, 1, 11, 33, 52, 139000),
    'invalid_value', 'is not datetime with', datetime(2021, 4, 1, 11, 33, 52, 139000)
)
timedelta_field_days_test_case = ParametrizedTestCase(
    TimedeltaField, {'units': 'days'}, '1', timedelta(days=1),
    'invalid_value', 'is not timedelta', timedelta(days=2)
)
timedelta_field_seconds_test_case = ParametrizedTestCase(
    TimedeltaField, {'units': 'seconds'}, '1', timedelta(seconds=1),
    'invalid_value', 'is not timedelta', timedelta(seconds=2)
)
timedelta_field_microseconds_test_case = ParametrizedTestCase(
    TimedeltaField, {'units': 'microseconds'}, '1', timedelta(microseconds=1),
    'invalid_value', 'is not timedelta', timedelta(microseconds=2)
)
timedelta_field_milliseconds_test_case = ParametrizedTestCase(
    TimedeltaField, {'units': 'milliseconds'}, '1', timedelta(milliseconds=1),
    'invalid_value', 'is not timedelta', timedelta(milliseconds=2)
)
timedelta_field_minutes_test_case = ParametrizedTestCase(
    TimedeltaField, {'units': 'minutes'}, '1', timedelta(minutes=1),
    'invalid_value', 'is not timedelta', timedelta(minutes=2)
)
timedelta_field_hours_test_case = ParametrizedTestCase(
    TimedeltaField, {'units': 'hours'}, '1', timedelta(hours=1),
    'invalid_value', 'is not timedelta', timedelta(hours=2)
)
timedelta_field_weeks_test_case = ParametrizedTestCase(
    TimedeltaField, {'units': 'weeks'}, '1', timedelta(weeks=1),
    'invalid_value', 'is not timedelta', timedelta(weeks=2)
)
timedelta_field_invalid_units_test_case = ParametrizedTestCase(
    TimedeltaField, {'units': 'invalid_units'}, '1', timedelta(days=1),
    'invalid_value', 'is incorrect units choice', timedelta(days=2)
)
attr_field_test_case = ParametrizedTestCase(
    AttrField, {'obj': AttrTest}, 'some_attr', AttrTest.some_attr, 'invalid_value', 'has no attribute', 'default_value'
)
split_list_field_test_case = ParametrizedTestCase(
    ListField, {'d_field': StrField()}, 'test_value_1,test_value_2', ['test_value_1', 'test_value_2'], None, None,
    ['default_value']
)
preloaded_list_field_test_case = ParametrizedTestCase(
    ListField, {'d_field': StrField()}, ['test_value_1', 'test_value_2'], ['test_value_1', 'test_value_2'],
    object, 'Is not iterable', ['default_value']
)
split_tuple_field_test_case = ParametrizedTestCase(
    TupleField, {'d_field': StrField()}, 'test_value_1,test_value_2', ('test_value_1', 'test_value_2'), None, None,
    ('default_value',)
)
preloaded_tuple_field_test_case = ParametrizedTestCase(
    TupleField, {'d_field': StrField()}, ['test_value_1', 'test_value_2'], ('test_value_1', 'test_value_2'),
    object, 'Is not iterable', ('default_value',)
)
base64_field_test_case = ParametrizedTestCase(
    Base64EncodedField, {}, base64.b64encode(b'test_value').decode(), b'test_value', 'asd qwe', 'Base64 decode error',
    b'default_value'
)
nested_field_test_case = ParametrizedTestCase(
    NestedDataField, {'validator_cls': NestedValidator}, {'nested_field': 'test_value'}, {'nested_field': 'test_value'},
    {}, 'nested_field', {'nested_field': 'default_value'}
)


test_data = [
    any_field_test_case,
    str_field_test_case,
    int_field_test_case,
    float_field_test_case,
    bool_field_test_case,
    path_field_test_case,
    site_field_test_case,
    site_underscore_field_test_case,
    datetime_field_test_case,
    timedelta_field_days_test_case,
    timedelta_field_seconds_test_case,
    timedelta_field_microseconds_test_case,
    timedelta_field_milliseconds_test_case,
    timedelta_field_minutes_test_case,
    timedelta_field_hours_test_case,
    timedelta_field_weeks_test_case,
    attr_field_test_case,
    split_list_field_test_case,
    preloaded_list_field_test_case,
    split_tuple_field_test_case,
    preloaded_tuple_field_test_case,
    base64_field_test_case,
    nested_field_test_case,
]


test_invalid_value_data = [
    int_field_test_case,
    float_field_test_case,
    bool_field_test_case,
    site_field_test_case,
    site_underscore_field_test_case,
    datetime_field_test_case,
    timedelta_field_days_test_case,
    timedelta_field_seconds_test_case,
    timedelta_field_microseconds_test_case,
    timedelta_field_milliseconds_test_case,
    timedelta_field_minutes_test_case,
    timedelta_field_hours_test_case,
    timedelta_field_weeks_test_case,
    timedelta_field_invalid_units_test_case,
    attr_field_test_case,
    preloaded_list_field_test_case,
    preloaded_tuple_field_test_case,
    base64_field_test_case,
    nested_field_test_case,
]
