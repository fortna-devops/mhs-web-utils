import pytest

from mhs_web_utils.validation import Validator, StrField, ValidationError, SiteValidator

from shared_data import test_data


@pytest.mark.parametrize('test_case_data', test_data)
def test_valid(test_case_data):
    input_data = {'f': test_case_data.valid_input_value}

    class TestValidator(Validator):
        f = test_case_data.field_cls(**test_case_data.field_kwargs)

    v = TestValidator(input_data)
    assert v.validate() == {'f': test_case_data.expected_value}


@pytest.mark.parametrize('test_case_data', test_data)
def test_invalid(test_case_data):
    input_data = {'f': None}

    class TestValidator(Validator):
        f = test_case_data.field_cls(**test_case_data.field_kwargs)

    v = TestValidator(input_data)
    with pytest.raises(ValidationError):
        v.validate()


def test_callback():
    input_data = {'f': 'test_value'}
    suffix = '_:)'

    class TestValidator(Validator):
        f = StrField()

        def clean_f(self, value):
            return value + suffix

    v = TestValidator(input_data)
    assert v.validate() == {k: v+suffix for k, v in input_data.items()}


@pytest.mark.parametrize('test_case_data', test_data)
def test_inherited(test_case_data):
    input_data = {
        'f1': test_case_data.valid_input_value,
        'f2': test_case_data.valid_input_value,
    }

    class TestValidator(Validator):
        f1 = test_case_data.field_cls(**test_case_data.field_kwargs)

    class TestValidator2(TestValidator):
        f2 = test_case_data.field_cls(**test_case_data.field_kwargs)

    v = TestValidator2(input_data)
    assert v.validate() == {
        'f1': test_case_data.expected_value,
        'f2': test_case_data.expected_value,
    }


def test_site_validator():
    input_data = {
        'site': 'some_site'
    }

    v = SiteValidator(input_data)
    assert v.validate() == {
        'site': 'some_site',
        'customer': 'some'
    }


def test_site_no_customer_validator():
    input_data = {
        'site': 'some_site'
    }

    class NoCustomerValidator(SiteValidator):
        include_customer = False

    v = NoCustomerValidator(input_data)
    assert v.validate() == {
        'site': 'some_site'
    }
