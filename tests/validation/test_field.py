import pytest

from mhs_web_utils.validation import FieldValidationError

from shared_data import test_data, test_invalid_value_data


@pytest.mark.parametrize('test_case_data', test_data)
def test_valid_value(test_case_data):
    f = test_case_data.field_cls(**test_case_data.field_kwargs)
    assert f.clean(test_case_data.valid_input_value) == test_case_data.expected_value


@pytest.mark.parametrize('test_case_data', test_invalid_value_data)
def test_invalid_value(test_case_data):
    f = test_case_data.field_cls(**test_case_data.field_kwargs)
    with pytest.raises(FieldValidationError) as e:
        f.clean(test_case_data.invalid_input_value)
    assert test_case_data.expected_error in str(e.value)


@pytest.mark.parametrize('test_case_data', test_data)
def test_required(test_case_data):
    f = test_case_data.field_cls(**test_case_data.field_kwargs)
    with pytest.raises(FieldValidationError) as e:
        f.clean(None)
    assert 'This field is required' == str(e.value)


@pytest.mark.parametrize('test_case_data', test_data)
def test_optional(test_case_data):
    f = test_case_data.field_cls(required=False, **test_case_data.field_kwargs)
    assert f.clean(None) is None


@pytest.mark.parametrize('test_case_data', test_data)
def test_default(test_case_data):
    f = test_case_data.field_cls(default=test_case_data.default_value, **test_case_data.field_kwargs)
    value = f.clean(None)
    assert value == test_case_data.default_value
    assert isinstance(value, type(test_case_data.default_value))


@pytest.mark.parametrize('test_case_data', test_data)
def test_value_default(test_case_data):
    f = test_case_data.field_cls(default=test_case_data.default_value, **test_case_data.field_kwargs)
    assert f.clean(test_case_data.valid_input_value) == test_case_data.expected_value


@pytest.mark.parametrize('test_case_data', test_data)
def test_valid_choice(test_case_data):
    f = test_case_data.field_cls(choices=(test_case_data.expected_value,), **test_case_data.field_kwargs)
    assert f.clean(test_case_data.valid_input_value) == test_case_data.expected_value


@pytest.mark.parametrize('test_case_data', test_data)
def test_invalid_choice(test_case_data):
    f = test_case_data.field_cls(choices=(test_case_data.default_value,), **test_case_data.field_kwargs)
    with pytest.raises(FieldValidationError) as e:
        f.clean(test_case_data.valid_input_value)
    assert 'is not a valid choice' in str(e.value)


@pytest.mark.parametrize('test_case_data', test_data)
def test_default_valid_choice(test_case_data):
    f = test_case_data.field_cls(default=test_case_data.default_value, choices=(test_case_data.default_value,),
                                 **test_case_data.field_kwargs)
    assert f.clean(None) == test_case_data.default_value


@pytest.mark.parametrize('test_case_data', test_data)
def test_default_invalid_choice(test_case_data):
    f = test_case_data.field_cls(default=test_case_data.default_value, choices=(test_case_data.expected_value,),
                                 **test_case_data.field_kwargs)
    with pytest.raises(FieldValidationError) as e:
        f.clean(None)
    assert 'is not a valid choice' in str(e.value)
