import enum
import json
from decimal import Decimal
from datetime import date, datetime, timedelta
from typing import Optional, Dict, Union, List, Any, Type

from .base import Response


__all__ = ['Encoder', 'JSONResponse']


class Encoder(json.JSONEncoder):

    def default(self, o: Any) -> Union[str, int, float]:
        if isinstance(o, Decimal):
            return float(o)
        if isinstance(o, enum.Enum):
            return o.name
        if isinstance(o, timedelta):
            return o.total_seconds()
        if isinstance(o, datetime):
            return o.strftime('%Y-%m-%d %H:%M:%S.000')
        if isinstance(o, date):
            return o.strftime('%Y-%m-%d')
        return super().default(o)


class JSONResponse(Response):

    def __init__(self, data: Union[List, Dict], code: int = 200, headers: Optional[Dict[str, str]] = None,
                 encoder: Type[json.JSONEncoder] = Encoder):
        if headers is None:
            headers = {}
        headers['Content-Type'] = 'application/json'
        super().__init__(body=self._encode(data, encoder), code=code, headers=headers)

    @staticmethod
    def _encode(data: Union[List, Dict], encoder: Type[json.JSONEncoder], **kwargs) -> str:
        return json.dumps(data, cls=encoder, **kwargs)
