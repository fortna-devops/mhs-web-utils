from typing import Optional, Dict

from .base import Response


__all__ = ['BadRequestResponse', 'UnauthorizedResponse', 'ForbiddenResponse', 'NotFoundResponse',
           'MethodNotAllowedResponse']


class BadRequestResponse(Response):

    def __init__(self, body: str = 'BadRequest', headers: Optional[Dict[str, str]] = None):
        super().__init__(body=body, code=400, headers=headers)


class UnauthorizedResponse(Response):

    def __init__(self, body: str = 'Unauthorized', headers: Optional[Dict[str, str]] = None):
        super().__init__(body=body, code=401, headers=headers)


class ForbiddenResponse(Response):

    def __init__(self, body: str = 'Forbidden', headers: Optional[Dict[str, str]] = None):
        super().__init__(body=body, code=403, headers=headers)


class NotFoundResponse(Response):

    def __init__(self, body: str = 'Not Found', headers: Optional[Dict[str, str]] = None):
        super().__init__(body=body, code=404, headers=headers)


class MethodNotAllowedResponse(Response):

    def __init__(self, body: str = 'Method not allowed', headers: Optional[Dict[str, str]] = None):
        super().__init__(body=body, code=405, headers=headers)
