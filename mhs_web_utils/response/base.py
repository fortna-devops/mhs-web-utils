from typing import Optional, Dict, Any


__all__ = ['Response']


class Response:
    __default_headers = {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'text/plain'
    }

    def __init__(self, body: str = '', code: int = 200, headers: Optional[Dict[str, str]] = None):
        self._body = body
        self._code = code

        if headers is None:
            headers = {}

        self._headers = dict(self.__default_headers, **headers)

    def to_dict(self) -> Dict[str, Any]:
        return {
            'statusCode': str(self._code),
            'headers': self._headers,
            'body': self._body
        }
