from abc import ABC
from typing import ClassVar, Type, Dict, Any

from ...request import Request
from ...validation import Validator


__all__ = ['QueryValidationMixin']


class QueryValidationMixin(ABC):
    validator_cls: ClassVar[Type[Validator]]

    def _validate(self, request: Request) -> Dict[str, Any]:
        validator = self.validator_cls(request.query)
        return validator.validate()

    def authorize(self, request: Request):
        super().authorize(request=request)  # type: ignore
        self._validate(request=request)
