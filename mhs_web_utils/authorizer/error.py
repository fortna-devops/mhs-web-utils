__all__ = ['AuthorizationError']


class AuthorizationError(Exception):
    """
    Authorizer error
    """
