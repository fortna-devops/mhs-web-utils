import json
import os
from typing import Dict, Optional, Any, List, ClassVar

from jose import jwt, jwk, backends, JOSEError
from jose.utils import base64url_decode

from .token import Token
from ..mixins import QueryValidationMixin
from ..base import Authorizer
from ..error import AuthorizationError
from ...request import Request
from ...validation import SiteValidator, CustomerValidator


__all__ = ['CognitoAuthorizer', 'CognitoSiteAuthorizer', 'CognitoCustomerAuthorizer', 'CognitoAdminAuthorizer']


class CognitoAuthorizer(Authorizer):
    """
    Check that cognito token in the request is valid
    """

    jwks_path_var: ClassVar[str] = 'jwks_path'
    __keys: ClassVar[Optional[List[Dict[str, str]]]] = None

    auth_header_key: ClassVar[str] = 'authorization'
    kid_token_header_key: ClassVar[str] = 'kid'

    _token: Optional[Token] = None

    @classmethod
    def _get_keys(cls) -> List[Dict[str, str]]:
        if cls.__keys is None:
            with open(os.environ[cls.jwks_path_var], 'r') as f:
                cls.__keys = json.load(f)['keys']
        return cls.__keys

    def _get_public_key(self, token_kid: str) -> Optional[backends.RSAKey]:
        for key in self._get_keys():
            if key['kid'] == token_kid:
                return jwk.construct(key)
        return None

    def authorize(self, request: Request):
        if self.auth_header_key not in request.headers:
            raise AuthorizationError('Authorization header not found')

        auth_header = request.headers[self.auth_header_key]
        if not auth_header:
            raise AuthorizationError('Authorization header is empty')

        split_header = request.headers['authorization'].split(' ')
        if len(split_header) != 2:
            raise AuthorizationError('Authorization header is invalid')

        _, token_str = split_header

        split_token = token_str.rsplit('.', 1)
        if len(split_token) != 2:
            raise AuthorizationError('Authorization token is invalid')

        message, encoded_signature = split_token
        decoded_signature = base64url_decode(encoded_signature.encode('utf-8'))

        try:
            token_headers = jwt.get_unverified_headers(token_str)
            if self.kid_token_header_key not in token_headers:
                raise AuthorizationError('Key id not found')

            public_key = self._get_public_key(token_headers[self.kid_token_header_key])
            if public_key is None:
                raise AuthorizationError('Public key not found')

            if not public_key.verify(message.encode('utf-8'), decoded_signature):
                raise AuthorizationError('Signature verification failed')

            self._token = Token(jwt.get_unverified_claims(token_str))
        except JOSEError as e:
            raise AuthorizationError(f'Parse token exception for request: {request}') from e

    @property
    def token(self) -> Token:
        if self._token is None:
            raise AuthorizationError('You need to call "authorize" method at first')
        return self._token


class CognitoAdminAuthorizer(CognitoAuthorizer):
    """
    Check that user have admin permissions
    """

    def authorize(self, request: Request):
        super().authorize(request=request)

        if not self.token.has_admin_permissions:
            raise AuthorizationError('Forbidden')


class CognitoSiteAuthorizer(QueryValidationMixin, CognitoAuthorizer):
    """
    Check that user have permissions for particular site
    """

    validator_cls = SiteValidator

    def _validate(self, request: Request) -> Dict[str, Any]:
        kwargs = super()._validate(request=request)
        customer = kwargs['customer']
        site = kwargs['site']

        if customer not in self.token.customers:
            raise AuthorizationError(f'Forbidden for customer "{customer}"')

        site = site.replace('_', '-')
        if site not in self.token.groups:
            raise AuthorizationError(f'Forbidden for site "{site}"')

        return kwargs


class CognitoCustomerAuthorizer(QueryValidationMixin, CognitoAuthorizer):
    """
    Check that user have permissions for particular customer
    """

    validator_cls = CustomerValidator

    def _validate(self, request: Request) -> Dict[str, Any]:
        kwargs = super()._validate(request=request)
        customer = kwargs['customer']

        if customer not in self.token.customers:
            raise AuthorizationError(f'Forbidden for customer "{customer}"')

        return kwargs
