from typing import Dict, Any, List, Optional


class Token:
    """
    Representation of cognito user token
    """

    admin_group_name: str = 'admin'
    rename_groups: Dict[str, str] = {
        'dhl': 'dhl-miami',
        'fedex': 'fedex-louisville',
        'amazon': 'amazon-zappos'
    }

    def __init__(self, data: Dict[str, Any]):
        self._data = data
        self._groups: Optional[List[str]] = None
        self._customers: Optional[List[str]] = None

    def _parse_groups(self) -> List[str]:
        groups = self._data.get('cognito:groups', [])
        groups = list({group.lower() for group in groups})
        for i, group in enumerate(groups):
            if group in self.rename_groups:
                groups[i] = self.rename_groups[group]
        return groups

    @property
    def data(self) -> Dict[str, Any]:
        return self._data

    @property
    def groups(self) -> List[str]:
        if self._groups is None:
            self._groups = self._parse_groups()
        return self._groups

    @property
    def customers(self) -> List[str]:
        if self._customers is None:
            self._customers = []
            for group in self.groups:
                if '-' not in group:
                    continue
                customer = group.split('-')[0]
                if customer in self._customers:
                    continue
                self._customers.append(customer)
        return self._customers

    @property
    def has_admin_permissions(self) -> bool:
        return self.admin_group_name in self.groups

    @property
    def user_name(self) -> str:
        return self._data['cognito:username']

    def get_customer_groups(self, customer: str) -> List[str]:
        return [group for group in self.groups if group.split('-', 1)[0] == customer]
