import os
from hashlib import sha256
from typing import ClassVar, Optional, Dict

from ..base import Authorizer
from ..error import AuthorizationError
from ...request import Request


__all__ = ['SignatureAuthorizer']


class SignatureAuthorizer(Authorizer):

    secret_key_var: ClassVar[str] = 'secret'
    __secret: ClassVar[Optional[str]] = None

    @classmethod
    def _get_secret(cls) -> str:
        if cls.__secret is None:
            cls.__secret = os.environ[cls.secret_key_var]
        return cls.__secret

    def authorize(self, request: Request):
        query = request.query.copy()
        signature = query.pop('signature', None)

        if signature is None:
            raise AuthorizationError('Signature not found')

        if signature != self.sign(query):
            raise AuthorizationError('Invalid signature')

    @classmethod
    def sign(cls, data: Dict[str, str]) -> str:
        sign_data = [cls._get_secret()]
        for key in sorted(data.keys()):
            sign_data.append(data[key])

        signature_key = ''.join(sign_data)
        signature = sha256(signature_key.encode())
        return signature.hexdigest()
