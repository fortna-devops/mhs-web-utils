from abc import ABC, abstractmethod

from ..request import Request


__all__ = ['Authorizer']


class Authorizer(ABC):

    @abstractmethod
    def authorize(self, request: Request):
        """
        Check if request is authorized
        """
