from typing import Dict, Any, ClassVar

from . import Validator, StrField, FieldValidationError


__all__ = ['SiteField', 'SiteValidator', 'CustomerValidator']


class SiteField(StrField):

    def _clean(self, value: str) -> str:
        cleaned_value = super()._clean(value).replace('-', '_')
        if '_' not in cleaned_value:
            raise FieldValidationError('Must contain "-" or "_"')
        return cleaned_value


class SiteValidator(Validator):
    include_customer: ClassVar[bool] = True

    site = SiteField()

    def validate(self) -> Dict[str, Any]:
        data = super().validate()
        if self.include_customer:
            data['customer'] = data['site'].split('_')[0]
        return data


class CustomerValidator(Validator):
    customer = StrField()
