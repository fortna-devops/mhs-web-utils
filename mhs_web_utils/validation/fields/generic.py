from distutils.util import strtobool
from typing import Any

from .base import BaseFiled
from ..error import FieldValidationError


__all__ = ['AnyField', 'StrField', 'IntField', 'FloatField', 'BoolField']


class AnyField(BaseFiled):

    def _clean(self, value: Any) -> Any:
        return value


class StrField(BaseFiled):

    def _clean(self, value: str) -> str:
        return str(value)


class IntField(BaseFiled):

    def _clean(self, value: str) -> int:
        try:
            return int(value)
        except ValueError as e:
            raise FieldValidationError(f'"{value}" is not int') from e


class FloatField(BaseFiled):

    def _clean(self, value: str) -> float:
        try:
            return float(value)
        except ValueError as e:
            raise FieldValidationError(f'"{value}" is not float') from e


class BoolField(BaseFiled):

    def _clean(self, value: str) -> bool:
        """
        True values are y, yes, t, true, on and 1
        False values are n, no, f, false, off and 0
        """
        try:
            return strtobool(value)
        except ValueError as e:
            raise FieldValidationError(f'"{value}" is not bool') from e
