import base64
import binascii

from .base import BaseFiled
from ..error import FieldValidationError


__all__ = ['Base64EncodedField']


class Base64EncodedField(BaseFiled):

    def _clean(self, value: str) -> bytes:
        try:
            cleaned_value = base64.b64decode(value)
        except binascii.Error as e:
            raise FieldValidationError('Base64 decode error') from e
        return cleaned_value
