from typing import Any

from .base import BaseFiled
from ..error import FieldValidationError


__all__ = ['AttrField']


class AttrField(BaseFiled):

    def __init__(self, obj: object, **kwargs):
        self._obj = obj
        super().__init__(**kwargs)

    def _clean(self, value: str) -> Any:
        try:
            return getattr(self._obj, value)
        except AttributeError as e:
            raise FieldValidationError(f'{self._obj} has no attribute "{value}"') from e
