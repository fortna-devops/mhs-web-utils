from typing import Type
from pathlib import PurePath

from .base import BaseFiled


__all__ = ['PathField']


class PathField(BaseFiled):

    def __init__(self, path_cls: Type[PurePath] = PurePath, **kwargs):
        self._path_cls = path_cls
        super().__init__(**kwargs)

    def _clean(self, value: str) -> PurePath:
        return self._path_cls(value)
