from abc import ABC, abstractmethod
from typing import Any, Optional, Tuple

from ..error import FieldValidationError


__all__ = ['BaseFiled']


class BaseFiled(ABC):

    def __init__(self, *, required: bool = True, default: Optional[Any] = None, choices: Tuple[Any, ...] = ()):
        self._required = required
        self._default = default
        self._choices = choices

    def clean(self, value: Optional[str]) -> Any:
        if value is None:
            if self._required and self._default is None:
                raise FieldValidationError('This field is required')
            cleaned_value = self._default
        else:
            cleaned_value = self._clean(value)

        if self._choices and cleaned_value not in self._choices:
            raise FieldValidationError(f'"{cleaned_value}" is not a valid choice. Choices are {self._choices}')

        return cleaned_value

    @abstractmethod
    def _clean(self, value: Any) -> Any:
        """Override in inherited class to provide clean behavior"""
