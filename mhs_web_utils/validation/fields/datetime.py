from datetime import datetime, timedelta
from typing import Tuple

from .base import BaseFiled
from ..error import FieldValidationError


__all__ = ['DateTimeField', 'TimedeltaField']


class DateTimeField(BaseFiled):

    def __init__(self, value_format: str = '%Y-%m-%dT%H:%M:%S.%fZ', **kwargs):
        self._value_format = value_format
        super().__init__(**kwargs)

    def _clean(self, value: str) -> datetime:
        try:
            return datetime.strptime(value, self._value_format)
        except ValueError as e:
            raise FieldValidationError(f'"{value}" is not datetime with format "{self._value_format}"') from e


class TimedeltaField(BaseFiled):
    units_choices: Tuple[str, ...] = (
        'days',
        'seconds',
        'microseconds',
        'milliseconds',
        'minutes',
        'hours',
        'weeks'
    )

    def __init__(self, units: str, **kwargs):
        self._units = units
        super().__init__(**kwargs)

    def _clean(self, value: str) -> timedelta:
        if self._units not in self.units_choices:
            raise FieldValidationError(f'"{self._units}" is incorrect units choice')

        try:
            return timedelta(**{self._units: float(value)})  # type: ignore
        except ValueError as e:
            raise FieldValidationError(f'"{value}" is not timedelta') from e
