from typing import Any, Iterable, List, Tuple, overload

from .base import BaseFiled
from ..error import FieldValidationError


__all__ = ['ListField', 'TupleField']


class IterableField(BaseFiled):

    def __init__(self, d_field: BaseFiled, delimiter: str = ',', **kwargs):
        self._d_field = d_field
        self._delimiter = delimiter
        super().__init__(**kwargs)

    @overload
    def _clean(self, value: List) -> Iterable[Any]:
        """Interface for pre-loaded list"""

    @overload
    def _clean(self, value: str) -> Iterable[Any]:
        """Interface for string to split"""

    def _clean(self, value: Any) -> Iterable[Any]:
        if isinstance(value, list):
            items = value
        elif isinstance(value, str):
            items = value.split(self._delimiter)
        else:
            raise FieldValidationError('Is not iterable')

        return (self._d_field.clean(item) for item in items)


class ListField(IterableField):

    def _clean(self, value: Any) -> List[Any]:
        return list(super()._clean(value))


class TupleField(IterableField):

    def _clean(self, value: Any) -> Tuple[Any, ...]:
        return tuple(super()._clean(value))
