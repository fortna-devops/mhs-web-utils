from typing import Any, Type, Dict, TYPE_CHECKING

from .base import BaseFiled
from ..error import FieldValidationError, ValidationError

if TYPE_CHECKING:
    from ..validator import Validator  # pragma: no cover


__all__ = ['NestedDataField']


class NestedDataField(BaseFiled):
    def __init__(self, validator_cls: Type['Validator'], **kwargs):
        self._validator_cls = validator_cls
        super().__init__(**kwargs)

    def _clean(self, value: Dict[str, Any]) -> Dict[str, Any]:
        validator = self._validator_cls(value)
        try:
            data = validator.validate()
        except ValidationError as e:
            raise FieldValidationError(str(e)) from e
        return data
