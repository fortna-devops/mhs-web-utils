from typing import Dict


__all__ = ['FieldValidationError', 'ValidationError']


class FieldValidationError(Exception):
    pass


class ValidationError(Exception):

    def __init__(self, errors: Dict[str, str]):
        self.errors = errors
        super().__init__(str(self.errors))
