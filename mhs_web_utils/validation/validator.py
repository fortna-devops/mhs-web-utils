from typing import Any, Dict

from .fields.base import BaseFiled
from .error import FieldValidationError, ValidationError


__all__ = ['Validator']


class ValidatorMeta(type):

    def __new__(mcs, name, bases, namespace):
        cls = super().__new__(mcs, name, bases, namespace)
        fields = {name for name, value in namespace.items() if isinstance(value, BaseFiled)}
        for base in bases:
            for f_name in getattr(base, '__fields__', set()):
                value = getattr(cls, f_name, None)
                if isinstance(value, BaseFiled):
                    fields.add(f_name)
        cls.__fields__ = frozenset(fields)
        return cls


class Validator(metaclass=ValidatorMeta):

    def __init__(self, raw_data: Dict[str, Any]):
        self._raw_data = raw_data

    def validate(self) -> Dict[str, Any]:
        errors: Dict[str, str] = {}
        data: Dict[str, Any] = {}

        for field_name in self.__fields__:  # type: ignore
            field = getattr(self, field_name)
            value = self._raw_data.get(field_name)

            try:
                cleaned_value = field.clean(value)

                clean_callback = getattr(self, f'clean_{field_name}', None)
                if clean_callback and callable(clean_callback):
                    cleaned_value = clean_callback(cleaned_value)

            except FieldValidationError as e:
                errors[field_name] = str(e)
            else:
                data[field_name] = cleaned_value

        if errors:
            raise ValidationError(errors)

        return data
