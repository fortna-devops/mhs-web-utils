from typing import Callable

from .handler import Handler


__all__ = ['Route']


class Route:

    def __init__(self, check: Callable[[str], bool], handler: Handler):
        self._check = check
        self._handler = handler

    @property
    def check(self):
        return self._check

    @property
    def handler(self) -> Handler:
        return self._handler
