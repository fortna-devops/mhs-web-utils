from typing import Dict, Any, Optional


__all__ = ['Request']


class Request:
    """
    Represent request based on AWS API Gateway event and context
    """

    def __init__(self, event: Dict[str, Any], context: Optional[Dict[str, Any]] = None):
        self._event = event
        self._context = context

        self._method = event['httpMethod']
        self._path = event['path']
        self._query = event.get('queryStringParameters') or {}
        self._headers = self._process_headers(event.get('headers') or {})
        self._body = event.get('body') or ''

    def __repr__(self) -> str:
        return f'method: {self._method}, path: {self._path}, query: {self._query}, headers: {self._headers}, ' \
               f'body: {self._body}'

    @staticmethod
    def _process_headers(headers: Dict[str, str]) -> Dict[str, str]:
        return {name.lower(): value for name, value in headers.items()}

    @property
    def event(self) -> Dict[str, Any]:
        return self._event

    @property
    def method(self) -> str:
        return self._method

    @property
    def path(self) -> str:
        return self._path

    @property
    def query(self) -> Dict[str, Any]:
        return self._query

    @property
    def headers(self) -> Dict[str, str]:
        return self._headers

    @property
    def body(self) -> str:
        return self._body
