from mhs_rdb import Connector


__all__ = ['DBMixin']


class DBMixin:

    def __init__(self, connector: Connector):
        self._connector = connector
        super().__init__()

    @property
    def connector(self) -> Connector:
        return self._connector
