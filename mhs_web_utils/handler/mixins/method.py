from typing import Tuple

from ...request import Request
from ...response import Response, MethodNotAllowedResponse


__all__ = ['MethodMixin']


class MethodMixin:
    allowed_methods: Tuple[str, ...] = ()

    def handle(self, request: Request) -> Response:
        handler_name = f'{request.method.lower()}_handler'
        if request.method not in self.allowed_methods or not hasattr(self, handler_name):
            return MethodNotAllowedResponse()

        return getattr(self, handler_name,)(request)
