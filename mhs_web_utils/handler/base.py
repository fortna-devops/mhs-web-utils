from abc import ABC, abstractmethod

from typing import Type, ClassVar

from ..request import Request
from ..response import Response
from ..authorizer import Authorizer, CognitoSiteAuthorizer


__all__ = ['Handler']


class Handler(ABC):
    authorizer_cls: ClassVar[Type[Authorizer]] = CognitoSiteAuthorizer

    def __init__(self):
        self.authorizer = self.authorizer_cls()

    def dispatch(self, request: Request) -> Response:
        self.authorizer.authorize(request)
        return self.handle(request)

    @abstractmethod
    def handle(self, request: Request) -> Response:
        """
        Handle request
        :param request: request object
        :return: response object
        """
