import logging
import traceback
from typing import Optional, Dict, Any, Type

from .response import NotFoundResponse

from .request import Request
from .route import Route
from .response import Response, JSONResponse, ForbiddenResponse
from .validation import ValidationError
from .authorizer import AuthorizationError


__all__ = ['Resolver']


logger = logging.getLogger(__file__.split('.')[0].upper())


class Resolver:

    def __init__(self, *routes: Route, debug: bool = False):
        self._routes = routes
        self._debug = debug

    def _get_route(self, request: Request) -> Optional[Route]:
        for route in self._routes:
            if route.check(request.path):
                return route
        return None

    def _process_request(self, request: Request) -> Response:
        try:
            route = self._get_route(request)
            if not route:
                return NotFoundResponse()

            return route.handler.dispatch(request)

        except AuthorizationError:
            logger.exception('Authorization error occurred while processing request')
            return ForbiddenResponse()

        except ValidationError as e:
            logger.exception('Validation error occurred while processing request')
            return JSONResponse(e.errors, code=400)

        except Exception as e:  # pylint: disable=W0703
            if not self._debug:
                raise

            logger.exception('An error occurred while processing request')
            return JSONResponse({
                'error': str(e),
                'traceback': traceback.format_exc()
            }, code=502)

    def process(self, event: Dict[str, Any], context: Dict[str, Any],
                request_cls: Type[Request] = Request) -> Dict[str, Any]:
        request = request_cls(event, context)
        response = self._process_request(request)
        return response.to_dict()
